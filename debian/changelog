closql-el (2.1.0-1) unstable; urgency=medium

  * New upstream release
    - d/p/0001-Add_closql-pkg.el_file.patch: file dropped

 -- Matteo F. Vescovi <mfv@debian.org>  Sun, 29 Dec 2024 12:06:44 +0100

closql-el (2.0.0-1) unstable; urgency=medium

  * New upstream release
   - d/p/0001-Add_closql-pkg.el_file.patch: patch refreshed
  * debian/control: S-V 4.6.2 -> 4.7.0 (no changes needed)

 -- Matteo F. Vescovi <mfv@debian.org>  Sat, 28 Sep 2024 15:03:34 +0200

closql-el (1.2.1+git20231217.1.1b2ee60-3) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 17:49:03 +0900

closql-el (1.2.1+git20231217.1.1b2ee60-2) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.3.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 24 Jul 2024 21:23:33 +0900

closql-el (1.2.1+git20231217.1.1b2ee60-1) unstable; urgency=medium

  * Team upload.
  * New upstream snapshot.
  * d/copyright: update years, add upstream maintainer email.
  * d/control, d/watch: update upstream homepage.
  * Update patch adding pkg.el file (update dependencies and versions).
  * d/control:
    - Bump S-V to 4.6.2 (no changes required).
    - Remove unneeded Build-Depends (we do not byte-compile at build
      time).
    - Update elpa-closql dependencies.
    - Remove redundant version to elpa-closql Recommends.
  * d/rules: do not byte-compile at build time, remove unneeded envvars.
  * d/docs: Replace README.md with README.org.

 -- Aymeric Agon-Rambosson <aymeric.agon@yandex.com>  Mon, 01 Jan 2024 21:26:37 +0100

closql-el (1.2.1-3) unstable; urgency=medium

  * debian/rules: fix wrong target on make rule (Closes: #1026704)
  * debian/control: S-V bump 4.6.0 -> 4.6.1 (no changes needed)

 -- Matteo F. Vescovi <mfv@debian.org>  Fri, 06 Jan 2023 11:34:01 +0100

closql-el (1.2.1-2) unstable; urgency=medium

  * d/p/0001-Add_closql-pkg.el_file.patch refreshed

 -- Matteo F. Vescovi <mfv@debian.org>  Sun, 27 Feb 2022 09:14:13 +0100

closql-el (1.2.1-1) unstable; urgency=medium

  * New upstream release
  * debian/control: dropping Built-Using field
  * debian/copyright: extend copyright timestamps

 -- Matteo F. Vescovi <mfv@debian.org>  Fri, 25 Feb 2022 22:44:57 +0100

closql-el (1.2.0-1) unstable; urgency=medium

  * New upstream release
    - d/p/0001-Add_closql-pkg.el_file.patch: version updated
  * debian/control: S-V bump 4.5.1 -> 4.6.0 (no changes needed)

 -- Matteo F. Vescovi <mfv@debian.org>  Wed, 13 Oct 2021 23:19:26 +0200

closql-el (1.0.6-3) unstable; urgency=medium

  * debian/control: drop emacs25 from Enhances
  * d/p/0001-Add_closql-pkg.el_file.patch: refreshed

 -- Matteo F. Vescovi <mfv@debian.org>  Mon, 23 Aug 2021 22:02:51 +0200

closql-el (1.0.6-2) unstable; urgency=medium

  * Upload to unstable

 -- Matteo F. Vescovi <mfv@debian.org>  Tue, 17 Aug 2021 22:03:01 +0200

closql-el (1.0.6-1) experimental; urgency=medium

  * New upstream release
  * debian/control: S-V bump 4.5.0 -> 4.5.1 (no changes needed)
  * debian/patches/0001-Add_closql-pkg.el_file.patch: field added

 -- Matteo F. Vescovi <mfv@debian.org>  Fri, 25 Jun 2021 22:13:20 +0200

closql-el (1.0.4-2) unstable; urgency=medium

  * Upload to unstable

 -- Matteo F. Vescovi <mfv@debian.org>  Fri, 28 Aug 2020 00:42:32 +0200

closql-el (1.0.4-1) experimental; urgency=low

  * Initial release (Closes: #966092)

 -- Matteo F. Vescovi <mfv@debian.org>  Thu, 23 Jul 2020 00:22:28 +0200
